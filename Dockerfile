FROM node:12-alpine

WORKDIR /usr/app

RUN apk add --no-cache bash python git gcc g++ make openssh rsync

COPY package*.json ./

RUN npm install

COPY ./src ./src
COPY ./app-conf.json ./
COPY ./tsconfig.json ./
COPY ./webpack.config.js ./

RUN npm run build

RUN npm prune --production

CMD ["npm", "start"]