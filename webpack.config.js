const path = require('path');
const nodeExternals = require('webpack-node-externals');

const PRODUCTION = process.env.NODE_ENV === 'production';

module.exports = {
  entry: {
    app: './src/index.ts',
    job: './src/job.ts',
  },
  mode: PRODUCTION ? 'production' : 'development',
  target: 'node',
  output: {
    path: path.resolve(__dirname, 'bin'),
    filename: '[name].js',
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          'ts-loader',
        ]
      }
    ]
  },
  externals: [
    nodeExternals({
      whitelist: ['webpack/hot/dev-server']
    }),
  ],
  node: {
    __dirname: true,
    __firname: true,
  },
}
