# website-backups

Utility for backing up websites using rsync (w/o web interface yet)

> Node 12.16.0

#### Directories and Files
- `app-conf.json` - application config
- `src/` - source code
- `bin/` - executable files (built)
- `logs/` - logs
- `data/` - data
- `data/sources.txt` - list of sources to backup

#### Sources File Format
Each line should be written in the following format:

`(Unique Id) (Host) (Username) (Source) ?(meta json string)`

(meta json string) is options json:

```json
{
    "cron": "* * * * *"
}
```

#### Development Help

##### Docker
1. Build:
> docker build -t website-backups .
2. Run:
> docker run --name website-backups-01 --env-file .env -p 3000:3000 -v "host dir":/usr/app/data -d --rm website-backups
