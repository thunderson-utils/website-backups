import extend from 'extend';
import path from 'path';
import {
  createLogger,
  format,
  Logger as WinstonLogger,
  transports as WinstonTransports,
} from 'winston';
import 'winston-daily-rotate-file';

interface LoggerOpts {
  dir: string;

  debug?: boolean;
  level?: string;

  maxLogFiles?: number|string;
}

class Logger {
  static defaultOpts = {
    debug: false,
    level: 'info',
    maxLogFiles: '14d',
  };

  opts: LoggerOpts;
  winstonLogger: WinstonLogger;

  constructor(opts: LoggerOpts) {
    this.opts = extend({}, Logger.defaultOpts, opts);

    const transports = [
      new WinstonTransports.DailyRotateFile({
        level: 'error',
        filename: path.resolve(this.opts.dir, 'error-%DATE%.log'),
        datePattern: 'YYYY-MM-DD',
        maxFiles: this.opts.maxLogFiles,
      }),
      new WinstonTransports.DailyRotateFile({
        level: 'info',
        filename: path.resolve(this.opts.dir, 'combined-%DATE%.log'),
        datePattern: 'YYYY-MM-DD',
        maxFiles: this.opts.maxLogFiles,
      }),
    ] as any[];

    if (this.opts.debug) {
      transports.push(
        new WinstonTransports.Console({
          format: format.combine(
            format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSSZZ' }),
            format.colorize(),
            format.printf(({ level, message, timestamp }) => {
              return `[${timestamp}] ${level}: ${message}`;
            }),
          )
        })
      );
    }

    this.winstonLogger = createLogger({
      level: this.opts.level,
      format: format.combine(
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSSZZ' }),
        format.printf(({ level, message, timestamp }) => {
          return `[${timestamp}] ${level}: ${message}`;
        }),
      ),
      transports,
    });
  }

  private sanitize(str: string) {
    return str.replace(/^\s+|\s+$/g, '');
  }

  log(msg: string): void {
    this.winstonLogger.log({ level: 'info', message: this.sanitize(msg) });
  }

  warn(msg: string): void {
    this.winstonLogger.log({ level: 'warn', message: this.sanitize(msg) });
  }

  error(msg: string): void {
    this.winstonLogger.log({ level: 'error', message: this.sanitize(msg) });
  }
}

export default Logger;
