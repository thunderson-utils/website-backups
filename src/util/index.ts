import path from 'path';

export { autobind } from 'core-decorators';

import appConf from '../../app-conf.json';
export const config = {
  ...appConf,
  backupsDir: process.env.BACKUPS_DIR || path.resolve('../../data/backups'),
  sshIdentityPath: process.env.SSH_IDENTITY_PATH,
} as {
  backupsDir: string;
  sshIdentityPath: string;
  maxLogFiles?: number|string;
};

import Logger from './logger';
export const logger = new Logger({
  debug: process.env.NODE_ENV === 'development',
  dir: path.resolve(__dirname, '../../logs'),
  maxLogFiles: config.maxLogFiles,
});

import Data from './data';
export const data = new Data();

export * as misc from './misc';

