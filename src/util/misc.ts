import fs from 'fs';
import { homedir } from 'os';
import { dirname } from 'path';
import { spawn } from 'child_process';
import { NextFunction, Request, Response } from 'express';
import bytes from 'bytes';

export const normalizePath = (path: string): string => {
  return path.replace('~', homedir());
};

export const fsAccess = (path: string, mode: number = fs.constants.F_OK): Promise<boolean> => {
  return new Promise(resolve => {
    fs.access(normalizePath(path), mode, (err: Error) => {
      if (err) {
        return resolve(false);
      }
      return resolve(true);
    });
  });
};

export const fsWriteFile = (path: string, data: any): Promise<any> => {
  return new Promise((resolve, reject) => {
    fs.mkdir(dirname(normalizePath(path)), { recursive: true }, (err: Error) => {
      if (err) {
        return reject(err);
      }
      fs.writeFile(normalizePath(path), data, (err: Error) => {
        if (err) {
          return reject(err);
        }
        return resolve();
      });
    });
  });
};

export const fsReadFile = (path: string, options?: { encoding?: BufferEncoding; flag?: string; }): Promise<string|Buffer> => {
  return new Promise((resolve, reject) => {
    fsAccess(normalizePath(path), fs.constants.R_OK).then(exists => {
      if (!exists) {
        return reject(new Error('File does not exist'));
      }
      fs.readFile(normalizePath(path), options, (err: Error, data: string|Buffer) => {
        if (err) {
          return reject(err);
        }
        return resolve(data);
      });
    });
  });
};

export const fsLs = (path: string): Promise<fs.Dirent[]> => {
  return new Promise((resolve, reject) => {
    fs.readdir(normalizePath(path), { withFileTypes: true }, (err, items) => {
      if (err) {
        return reject(err);
      }
      return resolve(items);
    });
  });
};

export const fsMkDir = (path: string): Promise<string> => {
  return new Promise((resolve, reject) => {
    fs.mkdir(normalizePath(path), { recursive: true }, (err, path) => {
      if (err) {
        return reject(err);
      }
      return resolve(path);
    });
  });
};

export const fsDirSize = (path: string): Promise<number> => {
  return new Promise((resolve, reject) => {
    let result = '';
    const child = spawn('du', ['-ks', path]);
    child.stdout.on('data', data => {
      result += data.toString();
    });
    child.on('close', code => {
      if (code === 0) {
        const [sizeStr] = result.replace(/[\t\s]+/g, ' ').split(' ');
        return resolve(parseInt(sizeStr));
      } else {
        return reject(new Error(code.toString()));
      }
    });
  });
};

type RouteHandler = (req?: Request, res?: Response, next?: NextFunction) => any;
export const asynch = (fn: RouteHandler) => (req: Request, res: Response, next: NextFunction) => {
  Promise.resolve(fn(req, res, next))
    .catch(next);
};

export const format = {
  bytes,
};
