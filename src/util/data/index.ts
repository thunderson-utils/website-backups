import path from 'path';

import { logger } from '../index';
import { fsWriteFile, fsReadFile } from '../misc';

const DATA_PATH = path.resolve(__dirname, '../../../data');

export interface BackupSource {
  id: string;
  host: string;
  username: string;
  source: string;
  meta?: {
    cron?: string;
    name?: string;
    excludePatterns?: string[];
  };
}

interface BackupJob {
  id: string;
  size: number; // number of bytes
  time: number; // number of milliseconds
}

const SOURCES_FILE_HEADER = '# (Unique Id) (Host) (Username) (Source) ?(meta json string)';
const LASTJOBS_FILE_HEADER = '# (Unique Id) (Size (KB)) (Last Time)';

class Data {
  private sourcesPath: string;
  private lastJobsPath: string;

  constructor() {
    this.sourcesPath = path.resolve(DATA_PATH, 'sources.txt');
    this.lastJobsPath = path.resolve(DATA_PATH, 'jobs-last.txt');
  }

  _normalize(str: string) {
    return str.trim().replace(/[\s]+/g, '_');
  }

  //#region Sources

  async _writeSources(docs: BackupSource[]) {
    let text = `${SOURCES_FILE_HEADER}\r\n`;
    docs.forEach(doc => {
      const id = this._normalize(doc.id);
      const host = this._normalize(doc.host);
      const username = this._normalize(doc.username);
      const source = this._normalize(doc.source);
      const meta = doc.meta ? JSON.stringify(doc.meta) : '';
      const row = `${id} ${host} ${username} ${source} ${meta}`;
      text += `${row}\r\n`;
    });
    await fsWriteFile(this.sourcesPath, text);
  }

  async getSources(): Promise<BackupSource[]> {
    let dataString;
    try {
      dataString = (await fsReadFile(this.sourcesPath)).toString();
    } catch (e) {
      dataString = null;
    }

    if (dataString === null) {
      logger.warn('Sources file was not found');
      await fsWriteFile(this.sourcesPath, SOURCES_FILE_HEADER);
      logger.log(`Created sources file at ${this.sourcesPath}`);
      return [];
    }

    const lines = dataString.split(/\r\n|\r|\n/)
      .map(str => str.replace(/^\s+|\s+$/g, ''))
      .filter(str => str !== '' && !str.startsWith('#'));

    const sources = [];
    for (let i = 0; i < lines.length; i++) {
      const line = lines[i];
      const [id, host, username, source, ...rest] = line.split(' ');
      const metaStr = rest.join(' ');

      if (!id || !host || !username || !source) {
        continue;
      }

      let meta = {};
      try {
        meta = JSON.parse(metaStr);
      } catch (e) {
        meta = {};
      }

      sources.push({ id, host, username, source, meta });
    }

    return sources;
  }

  async getSourceById(id: string): Promise<BackupSource|undefined> {
    const list = await this.getSources();
    return list.find(source => source.id === id);
  }

  async saveSource(doc: BackupSource) {
    const list = await this.getSources();
    const index = list.findIndex(item => item.id === doc.id);
    if (index >= 0) {
      list.splice(index, 1, doc);
    } else {
      list.push(doc);
    }
    return this._writeSources(list);
  }

  async delSources(ids: string[]) {
    if (ids.length === 0) {
      return;
    }
    const list = await this.getSources();
    const newList = list.filter(item => !ids.includes(item.id));
    return this._writeSources(newList);
  }

  //#endregion

  //#region Last Jobs

  async _writeLastJobs(docs: BackupJob[]) {
    let text = `${LASTJOBS_FILE_HEADER}\r\n`;
    docs.forEach(doc => {
      const id = this._normalize(doc.id);
      const size = this._normalize(doc.size.toString());
      const time = this._normalize(doc.time.toString());
      const row = `${id} ${size} ${time}`;
      text += `${row}\r\n`;
    });
    await fsWriteFile(this.lastJobsPath, text);
  }

  async getLastJobs(): Promise<BackupJob[]> {
    let dataString;
    try {
      dataString = (await fsReadFile(this.lastJobsPath)).toString();
    } catch (e) {
      dataString = null;
    }

    if (dataString === null) {
      logger.warn('Last Jobs file was not found');
      await fsWriteFile(this.lastJobsPath, LASTJOBS_FILE_HEADER);
      logger.log(`Created last jobs file at ${this.lastJobsPath}`);
      return [];
    }

    const lines = dataString.split(/\r\n|\r|\n/)
      .map(str => str.replace(/^\s+|\s+$/g, ''))
      .filter(str => str !== '' && !str.startsWith('#'));

    const jobs = [];
    for (let i = 0; i < lines.length; i++) {
      const line = lines[i];
      const [id, sizeStr, timeStr] = line.split(' ');

      if (!id) {
        continue;
      }

      let size = parseInt(sizeStr);
      if (Number.isNaN(size)) {
        size = 0;
      }

      let time = parseInt(timeStr);
      if (Number.isNaN(time)) {
        time = 0;
      }

      jobs.push({ id, size, time });
    }

    return jobs;
  }

  async getLastJobBySourceId(id: string): Promise<BackupJob|undefined> {
    const list = await this.getLastJobs();
    return list.find(job => job.id === id);
  }

  async saveLastJob(doc: BackupJob) {
    const list = await this.getLastJobs();
    const index = list.findIndex(item => item.id === doc.id);
    if (index >= 0) {
      list.splice(index, 1, doc);
    } else {
      list.push(doc);
    }
    return this._writeLastJobs(list);
  }

  //#endregion
}

export default Data;
