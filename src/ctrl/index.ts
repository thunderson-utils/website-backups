import path from 'path';
import { spawn } from 'child_process';
import { CronJob, CronJobParameters } from 'cron';

import { config, data, logger, misc } from '../util';
import { BackupSource } from '../util/data';

export default class Controller {
  private jobs = {} as { [sourceId: string]: CronJob };

  constructor() {
    this.registerSource = this.registerSource.bind(this);
    this.init = this.init.bind(this);
    this.getJobs = this.getJobs.bind(this);
  }

  registerSource(source: BackupSource) {
    if (this.jobs[source.id]) {
      this.jobs[source.id].stop();
      delete this.jobs[source.id];
    }

    const now = new Date();
    now.setMilliseconds(now.getMilliseconds() + 1);
  
    let job: CronJob;
  
    const opts: CronJobParameters = {
      cronTime: now,
      onTick: function() {
        logger.log(`Job for source ${source.id} is created.`);
        const child = spawn('node', [
          path.resolve(__dirname, '../../bin/job.js'),
          source.id.toString(),
        ], {
          stdio: 'pipe',
        });
        child.stdout.pipe(process.stdout);
        child.stderr.pipe(process.stderr);
        child.on('close', code => {
          const nextRunText = `Next run: ${job.nextDate().format('YYYY-MM-DD HH:mm:ss')}.`;
          if (code === 0) {
            logger.log(`Job for source ${source.id} is completed. ${nextRunText}`);
          } else {
            logger.error(`Job for source ${source.id} is failed (code: ${code}). ${nextRunText}`);
          }
        });
      },
    };
  
    if (source.meta.cron) {
      opts.cronTime = source.meta.cron;
    }
  
    job = new CronJob(opts);
    job.start();
    this.jobs[source.id] = job;
  
    logger.log(`Job for source ${source.id} has been registered. Next run: ${job.nextDate().format('YYYY-MM-DD HH:mm:ss')}.`);
  }

  removeSource(id: string) {
    if (!this.jobs[id]) {
      return;
    }
    this.jobs[id].stop();
    delete this.jobs[id];
    logger.log(`Job for source ${id} and its schedule has been removed.`);
  }

  async init() {
    await misc.fsMkDir(config.backupsDir);
    const sources = await data.getSources();
    sources.forEach(source => this.registerSource(source));
  }

  async getJobs() {
    const lastJobs = await data.getLastJobs();
    return Object.keys(this.jobs).map(id => {
      const lastJob = lastJobs.find(job => job.id === id);
      return {
        id,
        size: lastJob ? misc.format.bytes(lastJob.size * 1024) : '-',
        lastDate: this.jobs[id].lastDate() || (lastJob && lastJob.time),
        nextDate: this.jobs[id].nextDate(),
      };
    });
  }
}
