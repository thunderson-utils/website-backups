import dotenv from 'dotenv';

dotenv.config();

import { config } from './util';

import WebApp from './web';
import Controller from './ctrl';

const main = async () => {
  const ctrl = new Controller();
  await ctrl.init();
  const app = new WebApp(ctrl);
  app.start();
};

console.log('Config', config);

main();
