import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';

import { config, data, logger } from '../util';
import { asynch, fsLs, fsReadFile } from '../util/misc';

import type Controller from '../ctrl';

export default class App {
  ctrl: Controller;

  constructor(ctrl: Controller) {
    this.ctrl = ctrl;
    this.start = this.start.bind(this);
  }

  start() {
    const app = express();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.set('view engine', 'pug');
    app.set('views', path.resolve(__dirname, './templates'));
    app.use('/static', express.static(path.resolve(__dirname, './public')));

    const jquery = path.resolve(__dirname, '../../node_modules/jquery/dist/jquery.min.js');
    app.use('/static/js/vendor/jquery.js', express.static(jquery));

    const bootstrapJs = path.resolve(__dirname, '../../node_modules/bootstrap/dist/js/bootstrap.min.js');
    app.use('/static/js/vendor/bootstrap.js', express.static(bootstrapJs));

    const bootstrapCss = path.resolve(__dirname, '../../node_modules/bootstrap/dist/css/bootstrap.min.css');
    app.use('/static/css/vendor/bootstrap.css', express.static(bootstrapCss));

    app.get('/', asynch(async (req, res) => {
      const jobs = await this.ctrl.getJobs();
      res.render('pages/home', { page: 'home', jobs });
    }));

    app.get('/sources', asynch(async (req, res) => {
      const list = await data.getSources();
      res.render('pages/sources', { page: 'sources', list, error: req.query.error });
    }));

    app.post('/sources', asynch(async (req, res) => {
      const { body } = req;

      const exists = await data.getSourceById(body.id);
      if (!exists && (!body.id || !body.host || !body.username || !body.source)) {
        return res.redirect(`/sources?error=${encodeURIComponent('Fill required fields')}`);
      }

      let meta = exists ? exists.meta : {};
      if (body.meta) {
        meta = JSON.parse(body.meta);
      }

      const doc = {
        id: body.id,
        host: body.host || exists.host,
        username: body.username || exists.username,
        source: body.source || exists.source,
        meta,
      };
      await data.saveSource(doc);
      this.ctrl.registerSource(doc);

      return res.redirect('/sources');
    }));

    app.delete('/api/sources', asynch(async (req, res) => {
      if (!req.body || !Array.isArray(req.body)) {
        return res.sendStatus(400);
      }

      await data.delSources(req.body);
      req.body.forEach(id => this.ctrl.removeSource(id));

      return res.sendStatus(200);
    }));

    app.get('/logs', asynch(async (req, res) => {
      const getList = async () => {
        const list = await fsLs(logger.opts.dir);
        return list.filter(item => item.isFile() && !item.name.startsWith('.')).map(item => item.name);
      };
      let list = [] as string[];
      try {
        list = await getList();
      } catch (e) {
        //
      }
      const data = [] as { date: string; files: string[]; }[];
      list.forEach(filename => {
        const [_, ...rest] = filename.split('-');
        const [date] = rest.join('-').split('.');
        let index = data.findIndex(item => item.date === date);
        if (index === -1) {
          data.push({ date, files: [] });
          index = data.length - 1;
        }
        data[index].files.push(filename);
      });
      data.sort((a, b) => b.date.localeCompare(a.date));
      res.render('pages/logs', { page: 'logs', data });
    }));

    app.get('/logs/:name', asynch(async (req, res) => {
      let content;
      let errorText;
      try {
        content = (await fsReadFile(path.resolve(logger.opts.dir, req.params.name))).toString();
        if (!content.trim()) {
          errorText = 'No content';
        }
      } catch (e) {
        errorText = 'Failed to load the log';
      }
      res.render('pages/log', { page: 'log', content, errorText });
    }));

    app.get('/config', asynch(async (req, res) => {
      let publicKey = 'ssh-keygen';
      try {
        publicKey = (await fsReadFile(`${config.sshIdentityPath}.pub`)).toString();
      } catch (e) {
        //
      }
      res.render('pages/config', { page: 'config', publicKey });
    }));

    app.listen(3000, () => {
      logger.log('App is running on port 3000');
    });
  }
}
