import path from 'path';
import Rsync from 'rsync';

import { config, data, logger, misc } from './util';

const run = async (id: string) => {
  const sources = await data.getSources();
  const item = sources.find(s => s.id === id);
  if (!item) {
    logger.error(`Source with id ${id} not found`);
    process.exit(1);
  }

  logger.log(`Job for source ${item.id} is started.`)

  const destPath = `${path.resolve(config.backupsDir)}/${item.id}`;
  let sync = new Rsync()
    .shell(`ssh -o StrictHostKeyChecking=no -o ConnectTimeout=10 -l ${item.username} -i ${config.sshIdentityPath}`)
    .flags('avz') // a (recursive + preserve permissions, modification times etc.), v (verbose), z (compression)
    .delete() // delete files from the destination directory if they are removed from the source
    .source(`${item.username}@${item.host}:${item.source}`)
    .destination(destPath);

  if (item.meta.excludePatterns && Array.isArray(item.meta.excludePatterns)) {
    sync = sync.exclude(item.meta.excludePatterns);
  }

  sync.output(
    data => logger.log(`sync ${item.meta.name || item.id}: ${data}`),
    data => logger.error(`sync ${item.meta.name || item.id}: ${data}`)
  );

  const onSuccess = async () => {
    let size;
    try {
      size = await misc.fsDirSize(destPath);
    } catch (e) {
      size = 0;
    }

    const job = {
      id: item.id,
      size,
      time: Date.now(),
    };
    await data.saveLastJob(job);
    process.exit(0);
  };
  const onError = (code: number, err: Error) => {
    logger.error(`(${code}) ${err.message}`);
    process.exit(1);
  };
  sync.execute((err, code, cmd) => {
    if (err) {
      onError(code, err);
    }
    onSuccess();
  });
};

const args = process.argv.slice(2);
const [id] = args;
if (!id) {
  logger.error('Provide source id as an argument');
  process.exit(1);
}

run(id);